import numpy as np
from scipy.spatial import ConvexHull
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from tqdm import tqdm

# Calculating the point distribution for different configurations A and B

def fibonacci(num_pts):
    points = []
    indices = np.arange(0, num_pts+1, dtype=float)
    for i in indices:
        phi = np.arccos(1 - i/num_pts)
        theta = np.pi * (1 + 5**0.5) * i
        points.append([np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(phi)])
    points = np.array(points)
    points = np.concatenate([points, -points])
    return points

def fibonacci_means(num_pts):
    points = []
    
    fibonacci_matrix = [[0, 0, 1], [1, 0, 0], [0, 1, 1]]
    w, v = np.linalg.eig(fibonacci_matrix)
    ev = [v[0,2], v[1,2], v[2,2]]
    ev = ev / ev[2]
    
    golden_mean = [np.real(ev[1]), np.real(ev[0])] 
    
    indices = np.arange(1, num_pts+1, dtype=float)
    for i in indices:
        phi = np.arccos((i*golden_mean[0])%1)
        theta = 2 * np.pi * ((i*golden_mean[1])%1)
        points.append([np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(phi)])    
    points = np.array(points)
    points = np.concatenate([points, -points])
    return (points)


# Calculating the convex hull volume

def convex_hull(points):
    hull = ConvexHull(points)
    return hull.volume

# Calculating the total energy of the point configuration as stated in Thomson's problem

def energy_config(points):
    energy = 0
    for i in range(len(points)):
        for j in range(len(points)):
            if j<i:
                energy += 1/np.linalg.norm(points[i]-points[j])
            else:
                break
    return energy


'''
# Display results

# Show point configuration on sphere for exemplary number of points

points_fibonacci = fibonacci(150)
points_means = fibonacci_means(150)

fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(points_fibonacci[:,0], points_fibonacci[:,1], points_fibonacci[:,2], c='r')
ax.scatter(points_means[:,0], points_means[:,1], points_means[:,2], c='y')
plt.show()

# Show difference in convex hull volume for different number of points per unit half sphere

hulls_B = []
hulls_A = []

for i in np.arange(5,200):
    points_B = fibonacci(i)
    points_A = fibonacci_means(i)
    hull_B = convex_hull(points_B)
    hull_A = convex_hull(points_A)
    hulls_B.append([i, hull_B])
    hulls_A.append([i, hull_A])

hulls_B = np.array(hulls_B)
hulls_A = np.array(hulls_A)

fig = plt.figure()
plt.plot(hulls_A[:,0], hulls_A[:,1]-hulls_B[:,1], color='black')
plt.xlabel("Number of points per half sphere")
plt.ylabel("Difference in volume of convex hulls")


# Show difference in total energy for different number of points per unit half sphere

energies_B = []
energies_A = []

for i in tqdm(np.arange(5,200)):
    points_B = fibonacci(i)
    points_A = fibonacci_means(i)
    energy_B = energy_config(points_B)
    energy_A = energy_config(points_A)
    energies_B.append([i, energy_B])
    energies_A.append([i, energy_A])

energies_B = np.array(energies_B)
energies_A = np.array(energies_A)

fig = plt.figure()
plt.plot(energies_A[:,0], energies_A[:,1]-energies_B[:,1], color="black")
plt.xlabel("Number of points per half sphere")
plt.ylabel("Difference in total energy")


# Depricated


# !THIS IS PROBABLY WRONG!
def fibonacci_B(num_pts):
    points = []
    indices = np.arange(0, num_pts+1, dtype=float)
    for i in indices:
        phi = (np.pi/2) * np.sqrt(i/num_pts)
        theta = np.pi * (1 + 5**0.5) * i
        points.append([np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(phi)])
    points = np.array(points)
    points = np.concatenate([points, -points])
    return (points)



'''